import React, { Component} from "react";
import {hot} from "react-hot-loader";
import "./Searchtab.css";
import URLAdress from "../URLadress"

class Searchtab extends Component{

	constructor(props){
		super(props);
		this.state = {
      		recipeCategory: [],
      		checkedCategory: []
    	};
    	this.categoryChanged = this.categoryChanged.bind(this);
    	this.searchareaChanged = this.searchareaChanged.bind(this);
	}

	componentWillMount(){
		let currentComponent = this;
		fetch("http://localhost:8080/categories")  
		  .then(  
		    function(response) {  
		      if (response.status !== 200) {  
		        console.log('Сервер возвращает ошибку. Status Code: ' +  
		          response.status);  
		        return;  
		      }

		      // Examine the text in the response  
		      response.json().then(function(data) {   
		        currentComponent.setState({recipeCategory: data});
		      });  
		    }  
		  )  
		  .catch(function(err) {  
		    console.log('Fetch Error :-S', err);  
		  });
	}

	categoryChanged(event) {
    console.log('выбрана категория');
  }

  searchareaChanged(event) {
    console.log('выбрана область поиска');
  }

	render(){
	    return(
	    	<div className="contentarea"> 
	    		<input type='text' name='search_text' className="searchline" placeholder="Поиск..."/>
		    	<div className='checkboxgroup'>
		    		<div className = 'legend'>Категории:</div>
		    		{this.state.recipeCategory ? this.state.recipeCategory.map((item) =>
                	<div>
                    	<input type='checkbox' value={item.id} onChange={this.categoryChanged} />
		    				<label>{item.name}</label>
                	</div>
	            	)
	            	: 'Загрузка...'}
	            </div>
	            <div className='checkboxgroup'>
		    		<div className = 'legend'>Поиска в</div>
		    		<div>
                    	<input type='checkbox' value={0} onChange={this.searchareaChanged} />
		    				<label>Глобальный</label>
                	</div>
	            	<div>
                    	<input type='checkbox' value={1} onChange={this.searchareaChanged} />
		    				<label>У себя</label>
                	</div>
                	<div>
                    	<input type='checkbox' value={2} onChange={this.searchareaChanged} />
		    				<label>В подписках</label>
                	</div>
		    	</div>
	    	</div>

	    	
	      )
	};
}

export default Searchtab;