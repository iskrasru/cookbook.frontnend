import React, { Component} from "react";
import {hot} from "react-hot-loader";
import "./Tabs.css";

class Tabs extends Component{

	constructor(props){
		super(props);
		this.state = {tabIndex: 'search'};
		this.tabSwitch = this.tabSwitch.bind(this);
	}

	tabSwitch(tabName){
		this.setState({tabIndex: tabName});
	}

  render(){
    return(
	   	<div className="container">
		<ul className="tabs">
			<li className= {this.state.tabIndex === 'search' ? "active" : null}><a href="#search" onClick={() => this.tabSwitch('search')}>Поиск</a></li>
			<li className= {this.state.tabIndex === 'recieptes' ? "active" : null}><a href="#recieptes" onClick={() => this.tabSwitch('recieptes')}>Рецепты</a></li>
			<li className= {this.state.tabIndex === 'subscriptiones' ? "active" : null}><a href="#subscriptiones" onClick={() => this.tabSwitch('subscriptiones')}>Подписки</a></li>
			<li className= {this.state.tabIndex === 'global' ? "active" : null}><a href="#global" onClick={() => this.tabSwitch('global')}>Глобальные</a></li>
		</ul>
	   </div>
    );
  }
}

export default Tabs;