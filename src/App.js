import React, { Component} from "react";
import {hot} from "react-hot-loader";
import "./App.css";
import Searchtab from "./search/Searchtab.js"
import Tabs from "./tabs/Tabs.js"

class App extends Component{

  render(){
    return(
    	<div>
	     	<Tabs />
			<Searchtab />
		</div>
    );
  }
}

export default App;